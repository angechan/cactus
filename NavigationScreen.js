var React = require('react-native');
var {
  StyleSheet,
  TabBarIOS,
  Image,
  StatusBarIOS
} = React;

var SignUpScreen = require('./SignUpScreen');
var LoadingScreen = require('./common/LoadingScreen');

const MOCK_USER = {
  username: 'sweetie_shelly',
  firstName: 'Shelly',
  lastName: 'Sweets',
  avatar: require('image!profile_img_03'),
  punchLine: 'Living reactively',
  description: 'For every action, there is an equal and opposite reaction',
  followers: 582,
  following: 1324,
  isFollowing: true
};

var NavigationScreen = React.createClass({
  propTypes: {
    selectedTab: React.PropTypes.string
  },

  getInitialState() {
    return {
      selectedTab: 'home'
    };
  },

  render() {
    var navigator = this.props.navigator;
    var Profile = require('./ProfileScreen');
    var Home = require('./HomeScreen');
    var Explore = require('./ExploreScreen');
    var ReactScreen = require('./ContentScreen');
    var Notifications = require('./NotificationsScreen');
    var image = require('image!recactus_tabnav_icon');

    var resolveAssetSource = require('resolveAssetSource');
    var imageuri =  resolveAssetSource(image).uri;

    StatusBarIOS.setHidden(false);
    
    return (
      <TabBarIOS
        tintColor="#81cec8"
        barTintColor="#ffffff"
        translucent={false}
        style={{marginBottom: -10}} >
        {this._renderTabItem('home', 'home', <Home navigator={navigator} />)}
        {this._renderTabItem('explore', 'search', <Explore navigator={navigator} />)}
        {this._renderTabItem('notifications', 'ios-bell', <Notifications navigator={navigator} />)}
        {this._renderTabItem('profile', 'android-person', <Profile user={MOCK_USER} navigator={navigator} />)}
      </TabBarIOS>
    );
  },

  _renderTabItem(tabName: string, iconName: string, screen: ReactElement) {
    var Icon = require('react-native-vector-icons/Ionicons');

    return (
      <Icon.TabBarItem
        selected = {this.state.selectedTab === tabName}
        iconName={iconName}
        iconSize={28}
        onPress = {() => {
            this.setState({
                selectedTab: tabName
            });
        }}>
        {screen}
      </Icon.TabBarItem>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7CDEDB'
  },
});

module.exports = NavigationScreen;