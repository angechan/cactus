'use strict';

var React = require('react-native');
var {
  CameraRoll,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} = React;

var CameraRollView = require('./CameraRollView');
var Header = require('./common/Header');
var defaultStyles = require('./style/DefaultStyles');

var CAMERA_ROLL_VIEW = 'camera_roll_view';

var UploadContentScreen = React.createClass({

  getInitialState() {
    return {
      groupTypes: 'SavedPhotos'
    };
  },

  render() {
    return (
      <View>
        <Header
          onLeftButtonPress={this.props.onLeftButtonPress}
          onRightButtonPress={this.props.onRightButtonPress} />
        <CameraRollView
          ref={CAMERA_ROLL_VIEW}
          groupTypes={this.state.groupTypes} />
      </View>

    );
  },
});

module.exports = UploadContentScreen;