'use strict';
 
var React = require('react-native');
var {
  Image,
  StyleSheet,
  View,
  StatusBarIOS
} = React;

var Header = require('./common/Header');

var HomeScreen = React.createClass({
  propTypes: {
    navigator: React.PropTypes.object
  },

  getInitialState() {
    return {
      isReact: true,
      cameraOn: true
    };
  },

  updateCameraState() {
    this.setState({ cameraOn: !this.state.cameraOn });
  },

  getLeftButton() {
    var image = require('image!camera_on');
    if (!this.state.cameraOn) {
      image = require('image!camera_off');
    }
    return <Image source={image} style={{marginTop: 5, width: 23, height: 15}} />;
  },

  renderHeader() {
    var ToggleButton = require('./common/ToggleButton');
    var menuButton = <Image source={require('image!menu')} style={{marginTop: 5, width: 15, height: 15}} />;

    return (
      <View>
        <Header
          statusBarStyle={styles.statusBarStyle}
          hasNoStatusBar={true}
          cameraOn={this.state.cameraOn}
          onLeftButtonPress={() => this.updateCameraState()}
          leftButton={this.getLeftButton()}
          onRightButtonPress={() => console.log('left')}
          rightButton={menuButton} />
        <View style={styles.toggle}>
          <ToggleButton text="REACTIONS" onPress={() => this.setState({ isReact: true })} isActive={this.state.isReact} />
          <ToggleButton text="CONTENT" onPress={() => this.setState({ isReact: false })} isActive={!this.state.isReact} />
        </View>
      </View>
    );
  },

  getContent() {
    if (this.state.isReact) {
      var EventFeed = require('./common/Feed');
      return (
        <EventFeed renderHeader={() => this.renderHeader()} navigator={this.props.navigator} row={require('./EventCell')} />
      );
    }

    var ContentScreen = require('./ContentScreen');
    return (
        <ContentScreen renderHeader={() => this.renderHeader()} navigator={this.props.navigator} />
    );
  },

  render() {
    StatusBarIOS.setHidden(false);

    return (
      <View style={styles.container}>
        <View style={styles.statusBar} />
        {this.getContent()}
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBarStyle: {
  },

  statusBar: {
    backgroundColor: 'rgba(5,205,180,1)',
    height: 20
  },
  toggle: {
    backgroundColor: '#f1f1f1',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 12
  }
});

module.exports = HomeScreen;