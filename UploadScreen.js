'use strict';
 
 /**
  * Screen that allows user to upload or capture content for reactions.
  */

var React = require('react-native');
var {
  StyleSheet,
  View,
  Component,
  TouchableHighlight
} = React;
var CameraRollExample = require('./CameraRollExample');
var IconButton = require('./common/IconButton');

var UploadScreen = React.createClass({
  render() {
    return (
      <View style={styles.container}>
        <CameraRollExample />
        <IconButton 
            name="ios-cloud-upload-outline"
            onPress={() => this._takePicture()}
            size='35'
            style={{color: '#FFFFFF'}} />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    flex: 1,
  },
  controlPanel: {
    backgroundColor: '#7CDEDB',
    alignItems: 'center',
    alignSelf: 'stretch',
    padding: 2,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
});
 
module.exports = UploadScreen;