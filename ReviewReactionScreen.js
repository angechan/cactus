'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  TextInput,
  Component,
  TouchableHighlight,
  TouchableOpacity,
  ListView,
  Image,
  StatusBarIOS,
  ScrollView
} = React;

import Video from 'react-native-video';

var defaultStyles = require('./style/DefaultStyles');
var Header = require('./common/Header');
var IconButton = require('./common/IconButton');

const S3_URL = 'http://recactus.s3-eu-west-1.amazonaws.com/reactions/';
const MOCK_REACTION = require('image!home_img_02');

var ReviewButton = React.createClass({
  render: function() {
    return (
      <TouchableHighlight onPress={this.props.onPress} style={this.props.buttonStyle}>
        <Text style={this.props.buttonTextStyle}>{this.props.text}</Text>
      </TouchableHighlight>
    );
  }
});

var ReviewReactionScreen = React.createClass({
  getInitialState() {
    return {
      isVideoSelected: false,
      isCommentSelected: false,
      comment: '',
      contentOffset: {x: 0, y: -346}
    };
  },

  _getHeading() {
    return (
        <Text style={[defaultStyles.text, {color: '#ffffff'}]}>
          Reacting to
          <Text style={{fontWeight: 'bold'}}> something</Text>
        </Text>
    );
  },

  render: function() {
    // TODO move this to the process action
    /*
    var xhr = new XMLHttpRequest();
    var s3_url = S3_URL + "vid.mp4";
    xhr.open('PUT', s3_url, true);
    xhr.setRequestHeader('Content-Type', 'video/mp4');
    xhr.setRequestHeader('x-amz-acl', 'bucket-owner-full-control');
    xhr.send({uri: this.props.uri});
    */
    StatusBarIOS.setHidden(true);

    var CaptureReactionScreen = require('./CaptureReactionScreen');
    var ContentScreen = require('./ContentScreen');

    // not used but the comment stuff is useful
    var video = (
      <Image source={MOCK_REACTION} style={[styles.image, { justifyContent: 'flex-end' }]}>
        <ScrollView ref="commentView" contentOffset={this.state.contentOffset} scrollEnabled={false} style={{backgroundColor: 'transparent'}}>
          {this._getComment()}
        </ScrollView>
      </Image>
      );

    if (this.props.uri) {
      video = (
        <Video 
          source={{uri: this.props.uri}} // Can be a URL or a local file.
          rate={1.0}                   // 0 is paused, 1 is normal.
          volume={0.0}                 // 0 is muted, 1 is normal.
          muted={false}                // Mutes the audio entirely.
          paused={false}               // Pauses playback entirely.
          resizeMode="cover"           // Fill the whole screen at aspect ratio.
          repeat={true}                // Repeat forever.
          onLoadStart={this.loadStart} // Callback when video starts to load
          onLoad={this.setDuration}    // Callback when video loads
          onEnd={this.onEnd}           // Callback when playback finishes
          style={styles.backgroundVideo}>
          
          <ScrollView ref="commentView" contentOffset={this.state.contentOffset} scrollEnabled={false} style={{backgroundColor: 'transparent'}}>
            {this._getComment()}
          </ScrollView>
        </Video>
        );
    }

    return (
      <View style={styles.container}>
        <Header
          customHeading={this._getHeading()}
          hasNoStatusBar={true}
          onLeftButtonPress={this.props.onLeftButtonPress}
          onRightButtonPress={this.props.onRightButtonPress} />
        <Image
          source={this.props.content}
          style={styles.image} />
        {video}
        {this._getPalette()}
      </View>
    );
  },

  _getPalette() {
    return (
      <View style={styles.palette}>
        {this._getCaptureMode()}
        {this._getCommentIcon()}
      </View>
    );
  },

  _getCommentIcon() {
    if (this.state.isVideoSelected) {
      return null;
    }

    return (
      <TouchableOpacity onPress={() => this._setComment()}>
        <Text style={[styles.paletteStyle, this.state.isCommentSelected && defaultStyles.colour]}>T</Text>
      </TouchableOpacity>
    );
  },

  _setComment() {
    var isCommentSelected = this.state.isCommentSelected;
    this.setState({
      isCommentSelected: !isCommentSelected
    });


    if (!isCommentSelected) {
      this.refs.commentInput.focus();
    }
  },

  _getComment() {
    if(!this.state.isCommentSelected) {
      return null;
    }

    return (
      <TextInput
        ref="commentInput"
        style={[defaultStyles.text, styles.textInput]}
        onChangeText={(comment) => this.setState({comment})}
        onFocus={() => this._offsetContent(-120)}
        onEndEditing={() => this._offsetContent(-222)}
        value={this.state.comment}
        placeholder="Write something"
        placeholderTextColor="#fff"
      />
    );
  },

  _offsetContent(offset) {
    this.setState({
      contentOffset: {x: 0, y: offset}
    });
  },

  _getCaptureMode() {
    var videoStyle = [styles.captureStyle, this.state.isVideoSelected && defaultStyles.colour];
    var cameraStyle = [styles.captureStyle, !this.state.isVideoSelected && defaultStyles.colour];

    return (
      <View style={styles.captureMode}>
        <IconButton name="ios-videocam" size={45} style={videoStyle} onPress={this._toggleCaptureMode} />
        <Text style={styles.paletteStyle}> / </Text>
        <IconButton name="camera" size={45} style={cameraStyle} onPress={this._toggleCaptureMode} />
      </View>
    );
  },

  _toggleCaptureMode() {
    var isCommentSelected = this.state.isCommentSelected;
    if (!this.state.isVideoSelected) {
      isCommentSelected = false;
    }
    this.setState({
      isVideoSelected: !this.state.isVideoSelected,
      isCommentSelected
    });
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  textInput: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    height: 30,
    color: '#fff',
    textAlign: 'center'
  },
  captureMode: {
    flexDirection: 'row',
  },
  palette: {
    paddingTop: 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#f1f1f1'
  },
  paletteStyle: {
    paddingTop: 5,
    color: '#cccccc',
    fontFamily: 'Times',
    fontSize: 32
  },
  captureStyle: {
    color: '#cccccc'
  },
  image: {
    alignSelf: 'center',
    height: 252,
    width: 376
  },
  button: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
  },
  buttons: {
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 16,
    color: '#fff'
  },
  backgroundVideo: {
    height: 270,
    width: 376
  }
});
 
module.exports = ReviewReactionScreen;