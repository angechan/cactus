'use strict';

var React = require('react-native');
var {
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Image
} = React;

var defaultStyles = require('./style/DefaultStyles');
var Icon = require('react-native-vector-icons/FontAwesome');
var SignUpScreen = require('./SignUpScreen');


var SocialButton = React.createClass({
  render: function() {
    return (
      <TouchableHighlight onPress={this.props.onPress}>
        <View style={styles.button}>
          <Icon name={this.props.iconName} size={15} style={styles.icon} />
          <Text style={[defaultStyles.text, styles.buttonText]}>{this.props.count}</Text>
        </View>
      </TouchableHighlight>
    );
  }
});

function getFavouritesCount(item: Object): {count: ?string} {
  if (!item) {
    return "0";
  }

  return item.favourites;
}

function getCommentsCount(item: Object): {count: ?string} {
  if (!item) {
    return "0";
  }

  return item.comments;
}

function getRepostsCount(item: Object): {count: ?string} {
  if (!item) {
    return "0";
  }

  return item.reposts;
}

var SocialButtons = React.createClass({
  render: function() {
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={this.props.onPress}>
          <View style={styles.button}>
            <Text style={[defaultStyles.text, styles.reactIcon]}>R</Text>
            <Text style={[defaultStyles.text, styles.buttonText]}>{getFavouritesCount(this.props.item)}</Text>
          </View>
        </TouchableHighlight>
        <SocialButton 
          iconName="eye"
          count={getFavouritesCount(this.props.item)}
          onPress={this.props.onPress} />
        <SocialButton
          iconName="comment-o"
          count={getCommentsCount(this.props.item)}
          onPress={this.props.onPress} />
        <SocialButton
          iconName="share"
          count={getRepostsCount(this.props.item)}
          onPress={this.props.onPress} />
        <SocialButton
          iconName="ellipsis-h"
          onPress={this.props.onPress} />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  reactIcon : {
    fontWeight: 'bold',
    fontSize: 17,
    color: '#ddd'
  },
  icon: {
    color: '#ddd'
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonText: {
    paddingLeft: 3,
    color: '#ddd',
  },
});

module.exports = SocialButtons;
