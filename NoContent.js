'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  Text,
  View
} = React;

var defaultStyles = require('./style/DefaultStyles');

var NoContent = React.createClass({
  render: function() {
    var message = this.props.message ? this.props.message : "No content";
    return (
      <View style={[styles.container, styles.centerText]}>
        <Text style={[defaultStyles.text, styles.NoContentText]}>{message}</Text>
      </View>
    );
  }
});


var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  centerText: {
    alignItems: 'center',
  },
  NoContentText: {
    marginTop: 80,
    color: '#888888',
  }
});

module.exports = NoContent;