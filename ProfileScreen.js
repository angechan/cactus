'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableHighlight
} = React;

var defaultStyles = require('./style/DefaultStyles');
var Header = require('./common/Header');
var EventFeed = require('./EventFeed');
var Icon = require('react-native-vector-icons/Ionicons');
var ToggleButton = require('./common/ToggleButton');

var TextBlock = React.createClass({
  render: function() {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', margin: 10}}>
        <Text style={[defaultStyles.highlightedText, {fontWeight: 'bold', fontSize: 14}]}>
          {this.props.title}
        </Text>
        <Text style={[defaultStyles.text, {paddingLeft: 3}]}>{this.props.description}</Text>
      </View>
    );
  }
});

var UserTextBlock = React.createClass({
  render: function() {
    return (
      <View style={{alignItems: 'center', margin: 10}}>
        <Text style={[defaultStyles.highlightedText, {fontWeight: 'bold', fontSize: 14}]}>
          {this.props.title}
        </Text>
        <Text style={[defaultStyles.text, {paddingLeft: 3}]}>{this.props.description}</Text>
      </View>
    );
  }
});

var ProfileScreen = React.createClass({
  render() {
    var name = this.props.user.firstName + ' ' + this.props.user.lastName;

    var profileSummary =
      <View style={styles.profileSummary}>
        <View style={styles.profileHeading}>
          <TextBlock title={this.props.user.followers} description='Followers' />
          <Image
            source={this.props.user.avatar}
            style={styles.avatar}/>
          <TextBlock title={this.props.user.following} description='Following' />
        </View>

        <UserTextBlock
          title={this.props.user.punchLine}
          description={this.props.user.description} />

        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
          <ToggleButton isActive={true} buttonText='My Reactions' />
          <ToggleButton isActive={false} buttonText='Uploaded Content' />
        </View>
      </View>;

    return (
      <View style={styles.container}>
        <Header />
        {profileSummary}
        <View style={defaultStyles.rowSeparator} />
        <EventFeed />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  description: {
    fontSize: 20,
    color: '#000000'
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 30
  },
  container: {
    flex: 1,
    alignItems: 'stretch'
  },
  profileHeading: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  profileSummary: {
    alignItems: 'stretch',
    paddingBottom: 10,
    paddingTop: 20,
    backgroundColor: '#f1f1f1'
  }
});
 
module.exports = ProfileScreen;