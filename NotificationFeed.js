var React = require('react-native');
var {
  Image,
  ListView,
  StyleSheet,
  View,
} = React;

var defaultStyles = require('./style/DefaultStyles');

var MOCKED_MOVIES_DATA = [
  {
    event: "liked your reaction.",
    eventUser: 'Vector',
    posters: {image: 'http://i.imgur.com/UePbdph.jpg'}
  },
  {
    event: "liked your reaction.",
    eventUser: 'Edith',
    posters: {image: 'http://i.imgur.com/UePbdph.jpg'}
  },
  {
    event: "liked your reaction.",
    eventUser: 'Dr. Nefario',
    posters: {image: 'http://i.imgur.com/UePbdph.jpg'}
  },
];

var NotificationFeed = React.createClass({
  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      })
    };
  },

  componentDidMount: function() {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(MOCKED_MOVIES_DATA),
      loaded: true,
    });
  },

  renderRow: function(
    item: Object,
    sectionID: number | string,
    rowID: number | string,
    highlightRowFunc: (sectionID: ?number | string, rowID: ?number | string) => void,
  ) {
    var NotificationCell = require('./NotificationCell');

    return (
      <NotificationCell
        onHighlight={() => highlightRowFunc(sectionID, rowID)}
        onUnhighlight={() => highlightRowFunc(null, null)}
        item={item}
        isContent={this.props.isContent} />
    );
  },

  renderSeparator: function(
    sectionID: number | string,
    rowID: number | string,
    adjacentRowHighlighted: boolean
  ) {
    return (
      <View key={"SEP_" + sectionID + "_" + rowID} 
        style={[styles.rowSeparator, {marginTop: 2, marginBottom: 2}]}/>
    );
  },

  render: function() {
    var NoEvents = require('./NoContent');

    if (this.state.dataSource.getRowCount() === 0) {
      return (
        <NoEvents message="No Events" />
      );
    }
    
    return (
      <ListView
        ref="listview"
        style={styles.listview}
        renderSeparator={this.renderSeparator}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={true}
        showsVerticalScrollIndicator={false} />
    );
  }
});

// Hacked listview paddingTop
var styles = StyleSheet.create({
  listview: {
    paddingTop: 10,
    backgroundColor: '#fff'
  },
  spinner: {
    width: 30,
  },
  scrollSpinner: {
    marginVertical: 20,
  },
  rowSeparator: {
    backgroundColor: '#f3f3f3',
    height: 2,
  }
});

module.exports = NotificationFeed;