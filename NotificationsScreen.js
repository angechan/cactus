'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View
} = React;

var Header = require('./common/Header');
var NotificationFeed = require('./NotificationFeed');

var NotificationsScreen = React.createClass({
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <NotificationFeed navigator={this.props.navigator} />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  }
});

module.exports = NotificationsScreen;