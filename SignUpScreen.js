/**
 * Sign up
 */
'use strict';

var React = require('react-native');
var {
  StyleSheet,
  StatusBarIOS,
  Text,
  TextInput,
  TouchableHighlight,
  View
} = React;

var defaultStyles = require('./style/DefaultStyles');

var TextInputWithHint = React.createClass({
  render: function() {
    return (
      <TextInput
        autoCapitalize="none"
        password={this.props.isPassword}
        placeholder={this.props.placeholder}
        onChangeText={(value) => this.props.onChangeText(value)}
        placeholderTextColor="#666"
        style={[defaultStyles.text, styles.inputBox]} />
    );
  }
});

var SubmitButton = React.createClass({
  render: function() {
    return (
      <TouchableHighlight onPress={this.props.onSubmit} style={styles.button}>
          <Text style={[defaultStyles.text, styles.buttonText]}>{this.props.buttonText}</Text>
      </TouchableHighlight>
    );
  }
});

var SignUpScreen = React.createClass({
  getInitialState() {
    return {
      loading: false
    };
  },

  _setUsername(username) {
    this.setState({
      username: username.trim(),
      userNotFound: false,
    });
  },

  _setEmail(email) {
    this.setState({
      email: email.trim(),
    });
  },

  _setPassword(password) {
    this.setState({
      password: password,
    });
  },

  _setConfirmPassword(password) {
    this.setState({
      confirmPassword: password,
    });
  },

  _login() {
    if (this.invalidLogin()) {
      return;
    }

    this.props.onLogin(this.state.username);
  },

  _signup() {
    if (this.invalidLogin()) {
      return;
    }

    if (!this.state.email) {
      this.setWarningMessage("Please provide a email.");
      return;
    }

    if (!this.passwordsMatch()) {
      this.setWarningMessage("Passwords do not match.");
      return;
    }

    this.props.onLogin(this.state.username);
  },

  invalidLogin() {
    if (!this.state.username) {
      this.setWarningMessage("Please provide a username.");
      return true;
    }

    if (!this.state.password) {
      this.setWarningMessage("Please provide a password.");
      return true;
    }

    this.setWarningMessage("");
    return false;
  },

  passwordsMatch() {
    return this.state.confirmPassword === this.state.password;
  },

  setWarningMessage(message) {
    this.setState({
      warningMessage: message
    });
  },

  render() {
    StatusBarIOS.setHidden(false);

    var warning = <View />

    if (this.state.warningMessage) {
      warning = <Text style={[defaultStyles.text, styles.warningMessage]}>{this.state.warningMessage}</Text>
    }

    return (
      <View style={[defaultStyles.background, styles.container]}>
        <Text style={[defaultStyles.text, styles.header]}>Recactus</Text>
        {warning}
        <TextInputWithHint placeholder="username" onChangeText={this._setUsername} isPassword={false} />
        <TextInputWithHint placeholder="email" onChangeText={this._setEmail} isPassword={false} />
        <TextInputWithHint placeholder="password" onChangeText={this._setPassword} isPassword={true} />
        <TextInputWithHint placeholder="re-enter password" onChangeText={this._setConfirmPassword} isPassword={true} />
        <SubmitButton buttonText="sign up" onSubmit={this._signup} />
        
        <TextInputWithHint placeholder="username/email" onChangeText={this._setUsername} isPassword={false} />
        <TextInputWithHint placeholder="password" onChangeText={this._setPassword} isPassword={true} />
        <SubmitButton buttonText="login" onSubmit={this._login} />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    padding: 35,
    flex: 1,
  },
  header: {
    color: 'white',
    fontSize: 50,
    fontWeight: 'bold',
    paddingBottom: 20,
    textAlign: 'center',
  },
  inputBox: {
    height: 50,
    backgroundColor: 'white',
    flex: 1,
    fontSize: 14,
    padding: 15,
  },
  label: {
    color: 'white',
    width: 120,
    justifyContent: 'flex-end',
    marginRight: 10,
    marginTop: 10,
  },
  warningMessage: {
    color: 'white',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#999',
    padding: 15,
    marginBottom: 30,
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  link: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingTop: 10,
    textAlign: 'center'
  },
});

module.exports = SignUpScreen;
