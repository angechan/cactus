'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  TextInput
} = React;

var Icon = require('react-native-vector-icons/Ionicons');
var defaultStyles = require('../style/DefaultStyles');

var SearchBar = React.createClass({
  render: function() {
    var backgroundStyle = [defaultStyles.background, styles.container];

    if (this.props.backgroundStyle) {
      backgroundStyle.push(this.props.backgroundStyle);
    }

    return (
      <View style={backgroundStyle}>
        <View style={styles.searchBar}>
          <Icon name="ios-search-strong" size={16} color="#8c8c8c" style={styles.searchIcon} />
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Search"
            style={styles.searchBarInput} />
        </View>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    paddingTop: 5,
    paddingBottom: 5
  },
  searchBarInput: {
    borderRadius: 3,
    fontSize: 11,
    paddingLeft: 5,
    paddingTop: 3,
    flex: 1,
    height: 20,
    backgroundColor: '#ffffff',
    color: '#8c8c8c'
  },
  searchBar: {
    borderRadius: 3,
    backgroundColor: '#ffffff',
    marginLeft: 15,
    marginRight: 15,
    alignSelf: 'stretch', // stretch the header 
    alignItems: 'center', // center the text
    flexDirection: 'row'
  },
  searchIcon: {
    backgroundColor: 'rgba(0,0,0,0)',
    paddingTop: 3,
    paddingLeft: 10,
    paddingRight: 5,
    paddingBottom: 1
  }
});
 
module.exports = SearchBar;