'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Image,
  TouchableOpacity
} = React;

var Icon = require('react-native-vector-icons/Ionicons');
var defaultStyles = require('./../style/DefaultStyles');

var Header = React.createClass({
  getLeftButton: function() {
    if (!this.props.onLeftButtonPress) {
      return (<View />);
    }

    var button = <Icon name='android-close' size={20} style={{color: '#fff'}} />

    if (this.props.leftButton) {
      button = this.props.leftButton;
    }

    return (
      <TouchableOpacity onPress={this.props.onLeftButtonPress}>
        <View style={styles.button}>
          {button}
        </View>
      </TouchableOpacity>
    );
  },

  getRightButton: function() {
    if (!this.props.onRightButtonPress) {
      return (<View />);
    }

    var button = <Icon name='ios-arrow-right' size={20} style={{color: '#fff'}} />;

    if (this.props.rightButton) {
      button = this.props.rightButton;
    }

    return (
      <TouchableOpacity onPress={this.props.onRightButtonPress}>
        <View style={styles.button}>
          {button}
        </View>
      </TouchableOpacity>
    );
  },

  render: function() {
    if (this.props.noHeader) {
      return (
        <View style={[defaultStyles.background, styles.noHeader]} />
      );
    }

    var headerStyle = [defaultStyles.background, styles.header];

    if (this.props.statusBarStyle) {
      headerStyle.push(this.props.statusBarStyle);
    } else if (this.props.hasNoStatusBar) {
      headerStyle.push(styles.noStatusBar);
    } else {
      headerStyle.push(styles.statusBar);
    }

    if (this.props.customHeading) {
      return (
        <View style={[headerStyle, styles.header, styles.customHeading]}>
          {this.getLeftButton()}
          {this.props.customHeading}
          {this.getRightButton()}
        </View>
      );
    }

    var image = require('image!recactus_heading');
    return (
      <View style={[defaultStyles.background, styles.header, {padding: 10, paddingTop: 13, paddingBottom: 2}]}>
        {this.getLeftButton()}
        <Image source={image} style={styles.image}/>
        {this.getRightButton()}
      </View>
    );
  }
});

var styles = StyleSheet.create({
  header: {
    alignSelf: 'stretch', // stretch the header 
    alignItems: 'center', // center the text
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  noHeader: {
    paddingTop: 20
  },
  statusBar: {
    paddingTop: 25,
    height: 52
  },
  noStatusBar: {
    padding: 12,
    height: 40
  },
  image: {
    height: 34,
    width: 146
  },
  customHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

module.exports = Header;