var React = require('react-native');
var {
  Image,
  ListView,
  StyleSheet,
  Text,
  View,
} = React;

var defaultStyles = require('../style/DefaultStyles');

var MOCKED_FEED_DATA = [
  {
    avatar: require('image!profile_img_01'),
    event: "reacted to",
    eventUser: 'Vector0',
    contentUser: 'Gru',
    favourites: '100',
    comments: '12',
    reposts: '1',
    comment: "Check this out",
    content: require('image!home_img_01'),
    reaction: require('image!home_img_02')
  },
  {
    avatar: require('image!profile_img_02'),
    event: "liked",
    eventUser: 'Edith1',
    contentUser: 'Bob',
    favourites: '100',
    comments: '12',
    reposts: '1',
    comment: "Check this out",
    content: require('image!home_img_03'),
    reaction: require('image!home_img_04')
  },
  {
    avatar: require('image!profile_img_03'),
    event: "commented on",
    eventUser: 'Dr. Nefario2',
    contentUser: 'Agnes',
    favourites: '100',
    comments: '12',
    reposts: '1',
    comment: "Check this out",
    content: require('image!home_img_05'),
    reaction: require('image!home_img_06')
  },
    {
    avatar: require('image!profile_img_01'),
    event: "reacted to",
    eventUser: 'Vector3',
    contentUser: 'Gru',
    favourites: '100',
    comments: '12',
    reposts: '1',
    comment: "Check this out",
    content: require('image!home_img_01'),
    reaction: require('image!home_img_02')
  },
  {
    avatar: require('image!profile_img_02'),
    event: "liked",
    eventUser: 'Edith4',
    contentUser: 'Bob',
    favourites: '100',
    comments: '12',
    reposts: '1',
    comment: "Check this out",
    content: require('image!home_img_03'),
    reaction: require('image!home_img_04')
  },
  {
    avatar: require('image!profile_img_03'),
    event: "commented on",
    eventUser: 'Dr. Nefario5',
    contentUser: 'Agnes',
    favourites: '100',
    comments: '12',
    reposts: '1',
    comment: "Check this out",
    content: require('image!home_img_05'),
    reaction: require('image!home_img_06')
  },
];

var EventFeed = React.createClass({

  pushScreen(screen) {
    this.props.navigator.replace({
      component: screen,
    });
  },

  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      reactingIndex: 0,
      source: MOCKED_FEED_DATA
    };
  },

  componentDidMount: function() {
    var source = this.state.source.slice();
    source.forEach(this.fillIsReacting);

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(source),
      loaded: true,
      source
    });
  },

  fillIsReacting: function(element, index, array) {
    var isReacting = (this.state.reactingIndex == index);
    array[index] = {
      ...array[index],
      isReacting: isReacting
    }
  },

  updateReactItem: function(rowID: number) {
    var source = this.state.source.slice();
    var oldReactingIndex = this.state.reactingIndex;

    if (oldReactingIndex != rowID) {
      source[oldReactingIndex] = {
          ...source[oldReactingIndex],
          isReacting: false
      };
    }
    source[rowID] = {
        ...source[rowID],
        isReacting: true
    };

    this.setState({
        dataSource: this.state.dataSource.cloneWithRows(source),
        reactingIndex: rowID,
        source
    });
  },

  renderRow: function(
    item: Object,
    sectionID: number | string,
    rowID: number | string,
    highlightRowFunc: (sectionID: ?number | string, rowID: ?number | string) => void) {
    var Row = this.props.row;

    var CaptureReactionScreen = require('../CaptureReactionScreen');

    return (
      <Row
        onHighlight={() => highlightRowFunc(sectionID, rowID)}
        onUnhighlight={() => highlightRowFunc(null, null)}
        item={item}
        onPress={() => this.pushScreen(CaptureReactionScreen)}
        isContent={this.props.isContent}
        navigator={this.props.navigator} />
    );
  },

  updateReactionTimers: function(visibleRows, changedRows) {
    const changedRowsMap = changedRows.s1;

    for (const key in changedRowsMap) {
      if (changedRowsMap[key]) {
        this.updateReactItem(key);
        return;
      }
    }
  },

  render: function() {
    var NoEvents = require('../NoContent');

    if (this.state.dataSource.getRowCount() === 0) {
      return (
        <NoEvents message="No Events" />
      );
    }
    
    return (
      <ListView
        ref="listview"
        style={styles.listview}
        renderHeader={this.props.renderHeader}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={true}
        showsVerticalScrollIndicator={false}
        onChangeVisibleRows={this.updateReactionTimers}
        />
    );
  }
});

var styles = StyleSheet.create({
  listview: {
    backgroundColor: '#fff',
    flex: 1
  },
  spinner: {
    width: 30,
  },
  scrollSpinner: {
    marginVertical: 20,
  }
});

module.exports = EventFeed;