'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} = React;

var defaultStyles = require('./../style/DefaultStyles');
var Icon = require('react-native-vector-icons/Ionicons');

var TagButton = React.createClass({
  render: function() {
    var style = [styles.button];

    if (this.props.isActive) {
      style.push(defaultStyles.background);
    }

    var textStyle = [styles.buttonText];
    if (this.props.isActive) {
      textStyle.push(styles.buttonTextActive);
    }

    var icon = null;
    if (this.props.iconUri) {
      icon = <Icon name={this.props.iconUri} size={13} color="#fff" style={{paddingRight: 5}} />;
    }

    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={style}>
          {icon}
          <Text style={textStyle}>{this.props.buttonText}</Text>
        </View>
      </TouchableOpacity>
    );
  }
});

var styles = StyleSheet.create({
  button: {
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    margin: 3,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#dddddd'
  },
  buttonText: {
    alignSelf: 'center',
    color: '#aaaaaa',
    fontWeight: 'bold',
    fontSize: 11
  },
  buttonTextActive: {
    color: '#ffffff'
  }
});
 
module.exports = TagButton;