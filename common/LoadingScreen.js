var React = require('react-native');

const {
  ActivityIndicatorIOS,
  StyleSheet,
  View,
} = React;

var LoadingScreen = React.createClass({
  render() {
    return (
      <View style={[
        styles.background,
        this.props.addBottomPadding ? styles.bottomBarMargin : null,
        this.props.styles,
      ]} >
        <ActivityIndicatorIOS color='#333333' size="large"/>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  bottomBarMargin:{
    marginBottom: 110,
  },
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around'
  },
});

module.exports = LoadingScreen;
