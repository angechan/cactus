'use strict';

var React = require('react-native');
var {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} = React;
var Icon = require('react-native-vector-icons/Ionicons');

var IconButton = React.createClass({
  render: function() {
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={this.props.onPress}>
        <View>
          <Icon name={this.props.name} size={this.props.size} style={this.props.style} />
        </View>
      </TouchableOpacity>
    );
  }
});


module.exports = IconButton;
