var React = require('react-native');
var {
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} = React;

var ToggleButton = require('./ToggleButton');
var TagButton = require('./TagButton');

 
var SearchSection = React.createClass({
  render: function() {
    return (
      <View style={styles.searchSection}>
        <TagsSection />
        <View style={styles.toggleSection}>
          <ToggleButton isActive={true} buttonText='Tag' />
          <ToggleButton isActive={false} buttonText='User' />
        </View>
      </View>
    );
  },
});

var TagsSection = React.createClass({
  render: function() {
    return (
      <View style={styles.tagSection}>
        <TagButton isActive={true} buttonText='Trending' iconUri='ios-pricetag' />
        <TagButton isActive={false} buttonText='#pretty' />
        <TagButton isActive={false} buttonText='#teens' />
        <TagButton isActive={false} buttonText='#yolo' />
      </View>
    );
  }
});

var styles = StyleSheet.create({
  searchSection: {
    backgroundColor: '#f1f1f1',
    paddingTop: 2,
    paddingBottom: 7
  },
  toggleSection: {
    alignSelf: 'center',
    flexDirection: 'row'
  },
  tagSection: {
    flexDirection: 'row',
    margin: 5
  }
});

module.exports = SearchSection;
