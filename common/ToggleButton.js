'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} = React;

var defaultStyles = require('../style/DefaultStyles');

var ToggleButton = React.createClass({
  render: function() {
    if (this.props.isActive) {
      return (
        <View style={[styles.toggleButton, styles.selectedToggleButton]}>
          <Text style={[styles.buttonText, defaultStyles.highlightedText]}>{this.props.text}</Text>
        </View>
      );
    }

    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.toggleButton}>
          <Text style={[defaultStyles.text, styles.buttonText]}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
});

var styles = StyleSheet.create({
  selectedToggleButton: {
    borderBottomColor: '#05cdb4',
    borderBottomWidth: 3
  },
  toggleButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingBottom: 8,
    flex: 1
  },
  buttonText: {
    color: '#a5a4a4',
    fontWeight: 'bold'
  },
});
 
module.exports = ToggleButton;