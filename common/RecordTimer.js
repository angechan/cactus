'use strict';

var React = require('react-native');
var {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} = React;

var TimerMixin = require('react-timer-mixin');
var defaultStyles = require('../style/DefaultStyles');
var Progress = require('react-native-progress').Bar;
var Camera = require('react-native-camera');
import Dimensions from 'Dimensions';

const VIDEO_CAPTURE_TIMEOUT_IN_MS = 5000;
const PROGRESS_REFRESH_INTERVAL_IN_MS = 100;
const WIDTH = Dimensions.get('window').width;

var RecordTimer = React.createClass({
  mixins: [TimerMixin],
  intervalId: (null: any),

  getInitialState() {
    return {
      progress: 0,
      isRecordingFinished: false,
      uri: '',
      cameraType: Camera.constants.Type.front,
      captureMode: Camera.constants.CaptureMode.video
    }
  },

  componentWillReceiveProps() {
    if (this.props.startTimer && this.state.progress == 0) {
      this._startRecording();
    }

    if (!this.props.startTimer && this.state.progress != 0) {
      this._stopRecording();
    }
  },

  _startRecording() {
    var progress = 0;
    this.intervalId = this.setInterval(() => {
      if (progress >= 1) {
        this._recordingFinished();
      }
      progress += PROGRESS_REFRESH_INTERVAL_IN_MS / VIDEO_CAPTURE_TIMEOUT_IN_MS;
      this.setState({ progress });
    }, PROGRESS_REFRESH_INTERVAL_IN_MS);

    var options = {
      target: 'cameraRoll',
      mode: 'video',
      totalSeconds: VIDEO_CAPTURE_TIMEOUT_IN_MS/1000
    }

    var _setURI = this._setURIState;
    this.refs.cam.capture(options, function(err, data) {
      _setURI(data);
    });

  },

  _setURIState(uri: string) {
    this.setState({ uri });
  },

  _stopRecording() {
    this.clearInterval(this.setIntervalrvalId);
    this.setState({ progress: 0 });
    this.refs.cam.stopCapture();
  },

  _recordingFinished() {
      this.setState({ progress: 0, isRecordingFinished: true });
      this.clearInterval(this.intervalId);
      this.refs.cam.stopCapture();
  },

  _onPress() {
    this._recordingFinished();
    this.props.onPress(this.state.uri);
  },

  render() {
    return (
      <TouchableOpacity onPress={this._onPress}>
        <Progress progress={this.state.progress}
            unfilledColor='transparent' color='#05cdb4'
            height={35} width={WIDTH}
            borderWidth={0} borderRadius={0}>

            <View style={styles.recordBar}>
              <Image source={require('image!next_bar_pattern')} style={{width: WIDTH, height: 30}}>
                <View style={styles.recordText}>
                  <Image source={require('image!next_bar_video_icon')} style={styles.recordIcon} />
                  <Text style={[defaultStyles.text, styles.record]}>Next</Text>
                </View>
              </Image>

            </View>
        </Progress>
        <Camera
          ref="cam"
          captureMode={this.state.captureMode}
          type={this.state.cameraType}
          style={{display: 'none'}} />
      </TouchableOpacity>
    );
  }
});

var styles = StyleSheet.create({
  record: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center'
  },
  recordIcon: {
    marginRight: 8,
    marginTop: 6,
    width: 17,
    height: 10
  },
  recordBar: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    top: 6
  },
  recordText: {
    flexDirection: 'row',
    alignSelf: 'center'
  }
});

module.exports = RecordTimer;