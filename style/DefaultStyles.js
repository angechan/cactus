var React = require('react-native');

const DEFAULT_COLOUR = '#05cdb4';
const DEFAULT_FONT_COLOUR = '#323232';
const DEFAULT_FONT_SIZE = 12;

var defaultStyles = React.StyleSheet.create({
  text: {
    fontFamily: 'Roboto',
    fontSize: DEFAULT_FONT_SIZE,
    fontWeight: 'normal',
    color: DEFAULT_FONT_COLOUR
  },
  highlightedText: {
    fontFamily: 'Roboto',
    fontSize: DEFAULT_FONT_SIZE,
    fontWeight: 'bold',
    color: DEFAULT_COLOUR
  },
  background: {
  	backgroundColor: DEFAULT_COLOUR
  },
  colour: {
  	color: DEFAULT_COLOUR
  },
  borderColour: {
    borderWidth: 1,
    borderColor: DEFAULT_COLOUR
  },
  rowSeparator: {
    backgroundColor: '#f3f3f3',
    height: 5,
  }
});

module.exports = defaultStyles;
