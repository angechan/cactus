Developer Guide
===================

Pre-requisites
---------------
* Watchman
* Flow
* NVM
* iojs
* XCode 6.3+
* iOS simulator


```
#!bash

brew install watchman
brew install flow

brew install nvm
echo "source $(brew --prefix nvm)/nvm.sh" >> ~/.profile

brew unlink node
brew install iojs
brew link iojs --force

nvm install iojs-v2
nvm alias default iojs-v2

// install latest npm
npm install -g npm@latest

"export PATH=$PATH:./node_modules/.bin"  >> ~/.profile
```

Running the app
----------------
1. Install the node modules
    npm install
2. Open the Cactus.xcodeproj in XCode
3. Build the project by pressing Cmd+R 

Customisations
------------------------------
* In react-native-camera change line 96 stopRecording() to stopCapture(). This is a bug
* In react-native enable thumbnails for pictures to improve performance
* In react-native-side-menu/styles.js comment out line 16 which sets the menu position to absolute. This is breaking positioning of menu items at flex-end.

Running the app on your phone
------------------------------
Update AppDelegate.m with the IP address of your computer.
To get the IP address run `ifconfig` and look for the inet address under `en0`

Archiving the app for deploying
-------------------------------
1. Open Xcode
2. In AppDelegate.m uncomment line 46 to generate a static bundle
3. Run `react-native bundle --platform ios --dev false --entry-file Cactus.ios.js --bundle-output main.jsbundle`
This will create the bundle main.jsbundle
5. Add main.jsbundle into the Cactus directory in Xcode (replace the existing bundle if necessary)
6. Select your 'iOS Device' as the target device for running the application
7. Update the build version
8. Go to Product > Archive

Troubleshooting
----------------
## Xcode complains about no distribution certificate when validating or submitting the archive to App Store ##
1. Go to Member Center > Certificates
2. Delete your distribution certificate
3. Follow the instructions to reate a new distribution certificate

## React Native bundle build fails with error ##
Debug the issue by editing the relevant class in react-native with logging output.
In the past this has been due to the bundle cli code looking for index.ios.js.
See `node_modules/react-native/local-cli/bundle.js`

## React-native bundler does not work ##
Try `node node_modules/react-native/local-cli/cli.js bundle --minify --out main.jsbundle`