
'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  Component,
  TouchableOpacity,
  ListView,
  Image,
  TextInput,
  StatusBarIOS
} = React;

var defaultStyles = require('./style/DefaultStyles');
var Header = require('./common/Header');
var ToggleButton = require('./common/ToggleButton');
var IconButton = require('./common/IconButton');
var SearchSection = require('./common/SearchSection');
var Icon = require('react-native-vector-icons/FontAwesome');

var ContentOption = React.createClass({
    getInitialState() {
    return {
      isSelected: false
    };
  },

  _onToggle() {
    this.setState({isSelected: !this.state.isSelected});
  },

  render: function() {
    if (this.state.isSelected) {
      return (
        <TouchableOpacity onPress={this._onToggle}>
          <View style={styles.optionOn}>
            <Icon name={this.props.contentType} size={22} style={{color: '#fff'}} />
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity onPress={this._onToggle}>
        <View style={styles.option}>
          <Icon name={this.props.contentType} size={22} style={{color: '#ddd'}} />
        </View>
      </TouchableOpacity>
    );
  },
});

var ContentTypeSection = React.createClass({
  render: function() {
    var facebook = <ContentOption contentType="facebook" />
    var vine = <ContentOption contentType="vine" />
    var twitter = <ContentOption contentType="twitter" />
    var tumblr = <ContentOption contentType="tumblr" />
    var reddit = <ContentOption contentType="reddit" />
    var instagram = <ContentOption contentType="instagram" />
    var more = <ContentOption contentType="ellipsis-h" />

    return (
      <View style={styles.contentTypeSection}>
        {facebook}
        {vine}
        {twitter}
        {tumblr}
        {reddit}
        {instagram}
        {more}
      </View>
    );
  },
});

var ContentScreen = React.createClass({
  propTypes: {
    username: React.PropTypes.string,
    navigator: React.PropTypes.object
  },

  getInitialState() {
    return {
      isContent: false,
      isReact: true
    };
  },

  componentWillReceiveProps(nextProps) {
    var username = nextProps;
    if (username !== this.props.username) {
      // TODO fill me in
    }
  },

  componentWillMount: function() {
    Icon.getImageSource('ios-close-empty', 45).then((source) => this.setState({ backIcon: source }));
    Icon.getImageSource('ios-arrow-right', 25).then((source) => this.setState({ nextIcon: source }));
  },

  _onUploadPress() {
    var UploadContentScreen = require('./UploadContentScreen');
    var navigator = this.props.navigator;
    if (navigator) {
      navigator.push({
        component: UploadContentScreen,
        passProps: {
          navigator,
          onLeftButtonPress: () => this.props.navigator.popToTop(),
          onRightButtonPress: () => this.props.navigator.popToTop()
        },
      });
    }
  },

  render: function() {
    var ReactFeed = require('./common/Feed');
    var content = <ReactFeed navigator={this.props.navigator} row={require('./ReactCell')} />
    
    return (
      <View style={styles.container}>
        {this.props.renderHeader()}
        <ContentTypeSection />
        {content}
      </View>
    );
  },
});

var styles = StyleSheet.create({
  contentTypeSection: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff'
  },
  searchBar: {
    backgroundColor: '#f1f1f1',
    paddingTop: 15
  },
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1'
  },
  toggle: {
    backgroundColor: '#f1f1f1',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 12
  },
  image: {
    height: 115,
    margin: 3,
    width: 115
  },
  buttonText: {
    color: '#a5a4a4',
    fontWeight: 'bold'
  },
  option: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    padding: 6,
    paddingTop: 8,
    backgroundColor: '#999'
  },
  optionOn: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    padding: 6,
    paddingTop: 8,
    backgroundColor: '#05cdb4'
  },
  contentTypeText: {
    fontWeight: 'normal',
    fontSize: 11
  }
});
 
module.exports = ContentScreen;