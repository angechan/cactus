'use strict';
 
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  TextInput,
  Component,
  TouchableHighlight,
  TouchableOpacity,
  ListView,
  Image,
  StatusBarIOS
} = React;

var Icon = require('react-native-vector-icons/Ionicons');
import Video from 'react-native-video';

var defaultStyles = require('./style/DefaultStyles');
var Header = require('./common/Header');
var IconButton = require('./common/IconButton');

var S3_URL = 'http://recactus.s3-eu-west-1.amazonaws.com/reactions/';
const MOCK_REACTION = require('image!home_img_02');

var SocialMediaButton = React.createClass({
  getInitialState() {
    return {
      isSelected: this.props.isSelected
    };
  },

  _toggleSelected() {
  	this.setState({
  		isSelected: !this.state.isSelected
  	});
  },

  render: function() {
  	return (
  	  <TouchableOpacity style={[styles.socialMediaButton, this.state.isSelected && defaultStyles.borderColour]} onPress={() => this._toggleSelected()}>
  	  	<Text style={[defaultStyles.text, styles.socialMediaText, this.state.isSelected && defaultStyles.colour]}>{this.props.name}</Text>
  	  </TouchableOpacity>
  	);
  }
});

var PostReactionScreen = React.createClass({
  getInitialState() {
    return {
      isFacebookSelected: true,
      isTwitterSelected: true,
      isTumblrSelected: true
    };
  },

  _getHeading() {
    return (
        <Text style={[defaultStyles.text, {color: '#ffffff'}]}>
          Reacting to
          <Text style={{fontWeight: 'bold'}}> something</Text>
        </Text>
    );
  },

  render: function() {
    // TODO move this to the process action
    /*
    var xhr = new XMLHttpRequest();
    var s3_url = S3_URL + "vid.mp4";
    xhr.open('PUT', s3_url, true);
    xhr.setRequestHeader('Content-Type', 'video/mp4');
    xhr.setRequestHeader('x-amz-acl', 'bucket-owner-full-control');
    xhr.send({uri: this.props.uri});
    */
    StatusBarIOS.setHidden(true);

    var CaptureReactionScreen = require('./CaptureReactionScreen');
    var ContentScreen = require('./ContentScreen');

    var video = 
      <Image source={MOCK_REACTION} style={[styles.image, { justifyContent: 'flex-end' }]}>
        {this._getComment()}
      </Image>

    if (this.props.uri) {
      video = <Video 
        source={{uri: this.props.uri}} // Can be a URL or a local file.
        rate={1.0}                   // 0 is paused, 1 is normal.
        volume={0.0}                 // 0 is muted, 1 is normal.
        muted={false}                // Mutes the audio entirely.
        paused={false}               // Pauses playback entirely.
        resizeMode="cover"           // Fill the whole screen at aspect ratio.
        repeat={true}                // Repeat forever.
        onLoadStart={this.loadStart} // Callback when video starts to load
        onLoad={this.setDuration}    // Callback when video loads
        onEnd={this.onEnd}           // Callback when playback finishes
        style={styles.backgroundVideo} />
    }

    return (
      <View style={styles.container}>
        <Header
          customHeading={this._getHeading()}
          hasNoStatusBar={true}
          onLeftButtonPress={this.props.onLeftButtonPress}
          onRightButtonPress={this.props.onRightButtonPress} />
        <Image
          source={this.props.content}
          style={styles.image} />
        {video}
        {this._getPalette()}
      </View>
    );
  },

  _getPalette() {
    return (
      <View style={styles.palette}>
      	<View style={styles.socialMediaButtons}>
	      	<SocialMediaButton name="Facebook" isSelected={true} />
	      	<SocialMediaButton name="Twitter" isSelected={false} />
	      	<SocialMediaButton name="Tumblr" isSelected={false} />
      	</View>

      	<TouchableOpacity style={[defaultStyles.background, styles.postButton]} onPress={() => this._post()}>
      		<Icon name="ios-upload-outline" size={25} style={styles.postIcon} />
      		<Text style={styles.postButtonText}>POST</Text>
      	</TouchableOpacity>
      </View>
    );
  },

  _post() {
  	var navigator = this.props.navigator;

  	if (navigator) {
  		navigator.popToTop();
  	}
  },

  _getComment() {
    if(!this.state.isCommentSelected) {
      return null;
    }

    return (
      <TextInput
        style={[defaultStyles.text, styles.textInput]}
        onChangeText={(comment) => this.setState({comment})}
        value={this.state.comment}
        placeholder="Write something"
        placeholderTextColor="#fff"
      />
    );
  },
});

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  textInput: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    height: 30,
    color: '#fff',
    textAlign: 'center'
  },
  captureMode: {
    flexDirection: 'row',
  },
  palette: {
    paddingTop: 20,
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: '#f1f1f1'
  },
  paletteStyle: {
    paddingTop: 5,
    color: '#cccccc',
    fontFamily: 'Times',
    fontSize: 32
  },
  captureStyle: {
    color: '#cccccc'
  },
  image: {
    alignSelf: 'center',
    height: 252,
    width: 376
  },
  button: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
  },
  buttons: {
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  socialMediaButtons: {
  	flexDirection: 'row',
  	justifyContent: 'space-around'
  },
  socialMediaButton: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    width: 115,
    alignItems: 'center',
    padding: 4,
    backgroundColor: '#fff'
  },
  socialMediaText: {
    fontSize: 15,
    color: '#8c8c8c'
  },
  postButton: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
    padding: 8,
    height: 42
  },
  postButtonText: {
  	paddingLeft: 10,
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  },
  postIcon: {
  	color: '#fff'
  },
  backgroundVideo: {
    height: 270,
    width: 376
  }
});
 
module.exports = PostReactionScreen;