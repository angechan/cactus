'use strict';

var React = require('react-native');
var {
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} = React;

var defaultStyles = require('./style/DefaultStyles');
var Icon = require('react-native-vector-icons/FontAwesome');

var NotificationCell = React.createClass({
  render: function() {
    var image = require('image!mockup');

    return (
      <View>
      <View style={styles.itemRow}>
        <View style={styles.eventRow}>
          <Image
            source={{uri: 'http://facebook.github.io/react/img/logo_og.png'}}
            style={styles.avatar}/>
          <View style={[styles.eventRow, {paddingTop: 4}]}>
            <Text style={[defaultStyles.highlightedText, {fontSize: 14}]}>{this.props.item.eventUser} </Text>
            <View style={[styles.eventRow, {paddingTop: 2}]}>
              <Text style={defaultStyles.text}>{this.props.item.event} </Text>
              <Text style={[defaultStyles.text, styles.time]}>1m</Text>
            </View>
          </View>
        </View>
        <Image source={image} style={styles.preview}/>
      </View>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  avatar: {
    height: 28,
    width: 28,
    borderRadius: 13,
    marginLeft: 10,
    marginRight: 10,
  },
  preview: {
    height: 30,
    width: 30
  },
  time: {
    fontSize: 12,
    color: '#aaaaaa',
  },
  eventRow: {
    flexDirection: 'row'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'nowrap',
  },
});

module.exports = NotificationCell;
