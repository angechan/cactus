
var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  NavigatorIOS,
  StatusBarIOS
} = React;

var Icon = require('react-native-vector-icons/Ionicons');
var SignUpScreen = require('./SignUpScreen');
var LoadingScreen = require('./common/LoadingScreen');

var Cactus = React.createClass({
  getInitialState() {
    StatusBarIOS.setStyle('light-content');

    return {
      username: "testUser",
      fetchingSettings: false,
    };
  },

  componentWillMount: function() {
    Icon.getImageSource('drag', 30).then((source) => this.setState({ menuIcon: source }));  
  },

  _initialRoute(){
    var NavigationScreen = require('./HomeScreen');

    var navigator = this.refs.navigator;

    return {
      component: NavigationScreen,
      title: 'Recactus',
      rightButtonIcon: this.state.menuIcon,
      onRightButtonPress: () => this._onMenuPress(),
      passProps: {
        username: this.state.username
      }
    };
  },

  _onMenuPress() {
    var navigator = this.refs.navigator;

    if (navigator) {
      navigator.replace(this._initialRoute());
    }
  },

  render() {
    StatusBarIOS.setHidden(false);

    if (this.state.fetchingSettings) {
      return this.renderLoading();
    }

    if (!this.state.username) {
      return this.renderSettings();
    }

    return (
      <NavigatorIOS
        ref="navigator"
        style={styles.container}
        initialRoute={this._initialRoute()}
        tintColor={'white'}
        barTintColor={'#05cdb4'}
        titleTextColor={'white'}
        shadowHidden={true}
        translucent={true} 
        navigationBarHidden={true} />
    );
  },

  renderLoading() {
    return <LoadingScreen />;
  },

  renderSettings() {
    return <SignUpScreen onLogin={this.setUsername} />;
  },

  setUsername(username) {
    this.setState({
      fetchingSettings: false,
      username
    });
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7CDEDB'
  },
});

AppRegistry.registerComponent('Cactus', () => Cactus);

