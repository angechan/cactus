'use strict';

var React = require('react-native');
var {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} = React;

var RecordTimer = require('./common/RecordTimer');
var defaultStyles = require('./style/DefaultStyles');
import Dimensions from 'Dimensions';

const WIDTH = Dimensions.get('window').width;


var EventCell = React.createClass({
  _reviewReaction(uri: string) {
    var ReviewReactionScreen = require('./ReviewReactionScreen');

    this.setState({ uri });

    var navigator = this.props.navigator;
    if (navigator) {
      navigator.push({
        component: ReviewReactionScreen,
        passProps: { 
          uri,
          navigator,
          content: this.props.item.content,
          onLeftButtonPress: () => this.props.navigator.popToTop(),
          onRightButtonPress: () => this._postReaction(),
        }
      });
    }
  },

  _postReaction() {
    var PostReactionScreen = require('./PostReactionScreen');
    var navigator = this.props.navigator;
    if (navigator) {
      navigator.push({
        component: PostReactionScreen,
        passProps: {
          uri: this.state.uri,
          navigator,
          content: this.props.item.content,
          onLeftButtonPress: () => this.props.navigator.pop()
        }
      });
    }
  },

  render: function() {
    var SocialButtons = require('./SocialButtons');
    var aspectRatio = 1440/1200;
    var imageContentStyle = styles.imageContent;
    
    return (
      <View style={{marginBottom: 5}}>
        <Image source={this.props.item.content} style={{width: WIDTH, height: WIDTH / aspectRatio}}>
          <View style={imageContentStyle}>
            <View style={styles.eventRow}>
              <Image
                source={this.props.item.avatar}
                style={styles.avatar}/>
              <Text style={[defaultStyles.text, styles.user]}>{this.props.item.contentUser}</Text>
            </View>
            </View>
        </Image>

        <Image source={this.props.item.reaction} style={{width: WIDTH, height: WIDTH / aspectRatio}}>
          <View style={imageContentStyle}>
            <View style={styles.eventRow}>
              <Image
                source={this.props.item.avatar}
                style={styles.avatar}/>
              <Text style={[defaultStyles.text, styles.user]}>{this.props.item.eventUser}</Text>
            </View>
            <SocialButtons item={this.props.item} onPress={this.props.onSocialButtonsPress} />
          </View>
        </Image>

        <RecordTimer index={this.props.item.eventUser} startTimer={this.props.item.isReacting} onPress={(data) => this._reviewReaction(data)}/>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  avatar: {
    height: 30,
    width: 30,
    borderRadius: 15
  },
  imageContentDim: {
    backgroundColor: 'rgba(255,255,255,.7)',
    justifyContent: 'space-between',
    flex: 1
  },
  imageContent: {
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
    flex: 1
  },
  eventRow: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 10
  },
  user: {
    color: '#ddd',
    paddingTop: 6,
    paddingLeft: 3
  }
});

module.exports = EventCell;
