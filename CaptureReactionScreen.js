'use strict';
 
var React = require('react-native');

var {
  StyleSheet,
  View,
  Text,
  Component,
  TouchableOpacity,
  Image,
  StatusBarIOS
} = React;

var TimerMixin = require('react-timer-mixin');
var Camera = require('react-native-camera');

var Progress = require('react-native-progress');

var IconButton = require('./common/IconButton');
var Header = require('./common/Header');

var VibrancyView = require('react-native-blur').BlurView;
var defaultStyles = require('./style/DefaultStyles');

var VIDEO_CAPTURE_TIMEOUT_IN_MS = 3000;
var PROGRESS_REFRESH_INTERVAL_IN_MS = 100;
var CAPTURE_COUNTDOWN_IN_SEC = 3;

var DoneButton = React.createClass({
  mixins: [TimerMixin],
  intervalId: (null: any),

  getInitialState() {
    return {
      progress: 0
    }
  },

  componentDidMount() {
    var progress = 0;
    this.intervalId = this.setInterval(() => {
      if (progress >= 1) {
        this._onPress();
      }
      progress += PROGRESS_REFRESH_INTERVAL_IN_MS / VIDEO_CAPTURE_TIMEOUT_IN_MS;
      this.setState({ progress });
    }, PROGRESS_REFRESH_INTERVAL_IN_MS);
  },

  _onPress() {
      this.setState({ progress: 0 });
      this.clearInterval(this.intervalId);
      console.log("cleared interval");
      this.props.onPress();
  },

  render() {
    return (    
      <TouchableOpacity style={{marginTop: 220}}
        onPress={this._onPress}>
        <Progress.Pie 
          progress={this.state.progress}
          size={90}
          unfilledColor='rgba(255, 255, 255, 0.5)' color='rgba(5, 205, 180, 0.5)'
          borderColor='#05cdb4' borderWidth={2} />
        <Text style={[defaultStyles.text, styles.doneText]}>DONE</Text>
      </TouchableOpacity>
    );
  }
});

var CaptureReactionScreen = React.createClass({
  mixins: [TimerMixin],
  timeoutId: (null: any),

  propTypes: {
    navigator: React.PropTypes.object
  },

  getInitialState() {
    return {
        cameraType: Camera.constants.Type.front,
        captureMode: Camera.constants.CaptureMode.video,
        isImageHidden: false,
        captureCountdown: CAPTURE_COUNTDOWN_IN_SEC,
        url: ''
      }
  },

  componentDidMount() {
    this._captureReaction();
  },

  _getReactImage() {
    if (this.state.isImageHidden) {
      return (
        <Image
          source={this.props.image}
          style={styles.image}>
          <VibrancyView blurType="light" style={styles.blurOverlay} />
        </Image>
      );
    }
    return (
      <Image source={this.props.content} style={styles.image} />
    );
  },

  _getImageOverlay() {
    if (this.state.captureCountdown === CAPTURE_COUNTDOWN_IN_SEC) {
      return ( <View /> );
    }
    return ( <Text style={[defaultStyles.text, styles.captureText]}>{this.state.captureCountdown}</Text> );
  },

  _getHeading() {
    return (
        <Text style={[defaultStyles.text, {color: '#ffffff'}]}>
          Reacting to
          <Text style={{fontWeight: 'bold'}}> something</Text>
        </Text>
    );
  },

  render() {
    StatusBarIOS.setHidden(true);

    return (
      <View style={styles.container}>
        <Header 
          customHeading={this._getHeading()}
          hasNoStatusBar={true}
          onLeftButtonPress={this.props.onLeftButtonPress}
          onRightButtonPress={this.props.onRightButtonPress} />
        {this._getReactImage()}
        <Camera
          ref="cam"
          captureMode={this.state.captureMode}
          type={this.state.cameraType}
          style={styles.camera}>
          <DoneButton ref="doneButton" onPress={() => this._stopCapture()} />
        </Camera>
      </View>
    );
  },

  _stopCapture() {
    if (__DEV__) {
      this._goToReviewScreen('data');
    }
    this.refs.cam.stopCapture();
  },

  _goToReviewScreen(uri: string) {
    var ReviewReactionScreen = require('./ReviewReactionScreen');
    
    var navigator = this.props.navigator;
    if (navigator) {
      navigator.push({
        component: ReviewReactionScreen,
        passProps: { 
          uri,
          navigator,
          content: this.props.content,
          onLeftButtonPress: () => this.props.navigator.popToTop(),
          onRightButtonPress: () => this._postReaction(),
        }
      });
    }
  },

  _postReaction() {
    var PostReactionScreen = require('./PostReactionScreen');
    var navigator = this.props.navigator;
    if (navigator) {
      navigator.push({
        component: PostReactionScreen,
        passProps: {
          navigator,
          content: this.props.content,
          onLeftButtonPress: () => this.props.navigator.pop()
        }
      });
    }
  },

  _switchCamera() {
    var state = this.state;
    state.cameraType = state.cameraType === Camera.constants.Type.back
      ? Camera.constants.Type.front : Camera.constants.Type.back;
    this.setState(state);
  },

  _captureReaction() {
    // if (!__DEV__) {
      // console.log("********* capturing reaction");
      // var options = {
      //   target: 'cameraRoll',
      //   mode: 'video',
      //   totalSeconds: VIDEO_CAPTURE_TIMEOUT_IN_MS/1000
      // }

      // var _goToReviewScreen = this._goToReviewScreen;
      // this.refs.cam.capture(options, function(err, data) {
      //   console.log("********* cam done");
      //   _goToReviewScreen(data);
      // });
    // }
  },

  _countDown() {
    var timeRemaining = this.state.captureCountdown;
    if (timeRemaining <= 1) {
      this._captureReaction();
      return;
    }

    this.setTimeout(
      () => this._countDown(),
      CAPTURE_COUNTDOWN_IN_SEC * 1000
    );

    this.setState({
      captureCountdown: timeRemaining - 1,
    });
  }
});

var styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'black',
    flex: 1,
  },
  doneText: {
    fontSize: 24,
    color: '#fff',
    paddingLeft: 16,
    paddingRight: 14,
    paddingTop: 30,
    paddingBottom: 25,
    textAlign: 'center',
    position: 'absolute',
    top: 0,
    right: 5
  },
  image: {
    height: 280,
    width: 376
  },
  captureText: {
    color: '#aaa',
    fontSize: 80,
    fontWeight: 'bold',
  },
  camera: {
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  blurOverlay: {
    flex: 1,
    backgroundColor: 'transparent'
  }
});
 
module.exports = CaptureReactionScreen;